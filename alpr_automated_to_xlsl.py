from openalpr import Alpr
import sys
import pandas as pd
import re
import os
from difflib import SequenceMatcher
import numpy as np

alpr = Alpr("in", "/etc/openalpr/openalpr.conf", "/usr/share/openalpr/runtime_data")
if not alpr.is_loaded():
    print("Error loading OpenALPR")
    sys.exit(1)

alpr.set_top_n(10)
alpr.set_default_region("md")

# assign arguments
images = sys.argv[1]
input_csv = sys.argv[2]
output_csv = sys.argv[3]

# loading input csv file
df = pd.read_csv(input_csv, skipinitialspace=True)


# method to check chars close by
def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


for file in os.listdir(images):

    print 'processing ' + file

    filename_without_extension = os.path.splitext(file)[0]

    results = alpr.recognize_file(images + file)

    plate_number = 0
    for plate in results['results']:
        plate_number += 1
        result_rank = 1

        check_charby = True
        ground_truth_match = False

        for candidate in plate['candidates']:

            image_index = df["Image"][df["Image"] == filename_without_extension].index.tolist()

            ground_truth = df.iloc[image_index]['Ground Truth'].values[0]

            # eliminate all special chars
            ground_truth = re.sub(r'[^a-zA-Z0-9 ]', r'', ground_truth)

            # updates Plate_Detected, Detection_Confidence, Detection_Rank and Pattern Match
            if candidate['plate'] == ground_truth:
                ground_truth_match = True
                df.set_value(image_index, 'Plate_Detected', ground_truth)
                df.set_value(image_index, 'Detection_Confidence', candidate['confidence'])
                df.set_value(image_index, 'Detection_Rank', result_rank)
                check_charby = False
                df.set_value(image_index, 'Close_by_1 character/number', np.NaN)
                df.set_value(image_index, 'Missed_2-3_character/numbers', np.NaN)

                if not candidate['matches_template']:
                    df.set_value(image_index, 'Error', 'no pattern')

            # update False_+ve
            if candidate['matches_template'] and not ground_truth_match:
                df.set_value(image_index, 'False_+ve', 'y')

            # update Close_by_1 character/number
            if similar(candidate['plate'], ground_truth) == 0.9 and check_charby:
                df.set_value(image_index, 'Close_by_1 character/number', 'y')
                check_charby = False

            # update Missed_2-3_character/numbers
            if (similar(candidate['plate'], ground_truth) == 0.8 or similar(candidate['plate'],
                                                                            ground_truth) == 0.7) and check_charby:
                df.set_value(image_index, 'Missed_2-3_character/numbers', 'y')
                check_charby = False

            result_rank += 1

# Call when completely done to release memory
df.to_csv(output_csv)
alpr.unload()